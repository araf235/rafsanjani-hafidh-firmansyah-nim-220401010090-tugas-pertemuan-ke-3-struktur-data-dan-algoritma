#include <iostream>
#include <string>

using namespace std;

int main() {
    int umur, tinggi, tarif = 0;
    string nama;

    cout << "Masukkan Nama: ";
    getline(cin, nama);

    cout << "Masukkan Umur: ";
    cin >> umur;

    cout << "Masukkan Tinggi (cm): ";
    cin >> tinggi;

    if (umur < 1) {
        cout << "Maaf " << nama << ", Anda tidak diizinkan masuk" << endl;
    } else if (umur < 3) {
        tarif = 30000;
        if (tinggi > 70) {
            tarif += 10000;
        }
    } else if (umur < 7) {
        tarif = 40000;
        if (tinggi > 120) {
            tarif += 15000;
        }
    } else if (umur < 10) {
        tarif = 50000;
        if (tinggi > 150) {
            tarif += 20000;
        }
    } else {
        tarif = 80000;
    }

    cout << "====================================================\n";
    cout << "Halo " << nama << ", tarif wahana adalah: Rp " << tarif << endl;
    cout << "====================================================\n";
    return 0;
}
