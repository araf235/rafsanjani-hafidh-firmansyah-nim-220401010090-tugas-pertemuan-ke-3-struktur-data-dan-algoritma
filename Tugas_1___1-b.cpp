#include <iostream>
#include <string>
using namespace std;

class AnggotaMafia {
public:
    string nama;
    int umur;
    string tempattinggal;
    double tabungan;

    AnggotaMafia(string _nama, int _umur, string _tempattinggal, double _tabungan) {
        nama = _nama;
        umur = _umur;
        tempattinggal = _tempattinggal;
        tabungan = _tabungan;
    }
};

int main() {
    string nama, tempattinggal;
    int umur;
    double tabungan;

    cout << "Masukkan Nama: ";
    getline(cin, nama);

    cout << "Masukkan Umur: ";
    cin >> umur;

    cout << "Masukkan Tempat Tinggal: ";
    cin.ignore();
    getline(cin, tempattinggal);

    cout << "Masukkan Jumlah Tabungan dalam Dollar: ";
    cin >> tabungan;

    AnggotaMafia anggota(nama, umur, tempattinggal, tabungan);
    cout << "===================================================\n";
    if (anggota.umur > 40 && (anggota.tempattinggal == "Nevada" || anggota.tempattinggal == "New York" || anggota.tempattinggal == "Havana") && anggota.tabungan > 10000000) {
        cout << anggota.nama << " kemungkinan adalah seorang anggota mafia dengan pangkat Don." << endl;
    } else if (anggota.umur >= 25 && anggota.umur <= 40 && (anggota.tempattinggal == "New Jersey" || anggota.tempattinggal == "Manhattan" || anggota.tempattinggal == "Nevada") && anggota.tabungan >= 1000000 && anggota.tabungan <= 2000000) {
        cout << anggota.nama << " kemungkinan adalah seorang anggota mafia dengan pangkat Underboss." << endl;
    } else if (anggota.umur >= 18 && anggota.umur <= 24 && (anggota.tempattinggal == "California" || anggota.tempattinggal == "Detroit" || anggota.tempattinggal == "Boston") && anggota.tabungan < 1000000) {
        cout << anggota.nama << " kemungkinan adalah seorang anggota mafia dengan pangkat Capo." << endl;
    } else {
        cout << anggota.nama << " tidak mencurigakan." << endl;
    }
    cout << "===================================================\n";

    return 0;
}
