#include <iostream>
using namespace std;

int main() {
    int nilai_koding;
    char nilai_interview;
    
    cout << "Masukkan Nilai Koding: ";
    cin >> nilai_koding;
    
    cout << "Masukkan Nilai Interview (A/B/C/D/E): ";
    cin >> nilai_interview;
    
    cout << "====================================================\n";
    
    if (nilai_koding >= 80) {
        cout << "|| Hasil Test Koding : LOLOS " << endl;
    } else if (nilai_koding >= 60) {
        cout << "|| Hasil Test Koding : DIPERTIMBANGKAN" << endl;
    } else {
        cout << "|| Hasil Test Koding : GAGAL" << endl;
    }
    
    if (nilai_interview == 'A' || nilai_interview == 'B') {
        cout << "|| Hasil Test Interview : LOLOS" << endl;
    } else {
        cout << "|| Hasil Test Interview : GAGAL" << endl;
    }
    
    if ((nilai_koding >= 60) && (nilai_interview == 'A' || nilai_interview == 'B')) {
        cout << "|| Selamat Kamu Berhasil Menjadi Calon Programmer!" << endl;
    } else {
        cout << "|| Maaf Kamu Belum Berhasil Menjadi Calon Programmer" << endl;
    }
    
    cout << "====================================================\n";
    
    return 0;
}
