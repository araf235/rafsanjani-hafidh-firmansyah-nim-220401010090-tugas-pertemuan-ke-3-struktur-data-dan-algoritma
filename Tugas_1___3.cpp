#include <iostream>
using namespace std;

int main() {
    string nama;
    int nomor_punggung;
    
    cout << "Masukkan Nama Pemain: ";
    cin >> nama;
    
    cout << "Masukkan Nomor Punggung: ";
    cin >> nomor_punggung;
    cout << "=========================================================\n";
    cout << "                       Persegi FC\n";
    cout << "=========================================================\n";
    
    if (nomor_punggung % 2 == 0) {
        // nomor punggung genap 50 - 100 
        if (nomor_punggung >= 50 && nomor_punggung <= 100) {
            cout << nama << " mendapatkan nomor punggung " << nomor_punggung << " sebagai Captain Team." << endl;
        // nomor punggung genap
        } else {
            cout << nama << " mendapatkan nomor punggung " << nomor_punggung << " sebagai Target Attacker." << endl;
        }
    } else {
        // nomor punggung ganjil kelipatan 3 dan 5
        if (nomor_punggung % 3 == 0 && nomor_punggung % 5 == 0) {
            cout << nama << " mendapatkan nomor punggung " << nomor_punggung << " sebagai Keeper." << endl;
        // nomor punggung ganjil > 90
        } else if (nomor_punggung > 90) {
            cout << nama << " mendapatkan nomor punggung " << nomor_punggung << " sebagai Playmaker." << endl;
        // nomor punggung ganjil
        } else {
            cout << nama << " mendapatkan nomor punggung " << nomor_punggung << " sebagai Defender." << endl;
        }
    }
    cout << "=========================================================\n";
    
    return 0;
}
