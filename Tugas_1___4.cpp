#include <iostream>
using namespace std;

int main() {
    string nama;
    int lari, push_up, plank, waktu_lari, waktu_push_up, waktu_plank, total_kalori = 0;
    cout << "====================================================\n";
    cout << "Olahraga Lari membakar 60 kalori setiap 5 menit. \n";
    cout << "Olahraga Push-up membakar 200 kalori setiap 30 menit \n";
    cout << "Olahraga Plank membakar 5 kalori selama 1 menit \n";
    cout << "====================================================\n";

    cout << "Masukkan Nama: ";
    getline(cin, nama);
    cout << "Masukkan Lama Waktu Berlari (detik): ";
    cin >> waktu_lari;
    cout << "Masukkan Lama Waktu Push-up (detik): ";
    cin >> waktu_push_up;
    cout << "Masukkan Lama Waktu Plank (detik): ";
    cin >> waktu_plank;

    // Menghitung jumlah kalori yang terbakar
    lari = (waktu_lari / 300) * 60;
    push_up = (waktu_push_up / 1800) * 200;
    plank = waktu_plank * 5 / 60;

    total_kalori = lari + push_up + plank;

    // Konversi detik ke menit dan detik
    int total_detik = waktu_lari + waktu_push_up + waktu_plank;
    int menit = total_detik / 60;
    int detik = total_detik % 60;

    cout << "====================================================\n";
    cout << "|| Nama: " << nama << endl;
    cout << "|| Lama Waktu Olahraga: " << menit << " Menit " << detik << " Detik" << endl;
    cout << "|| Jumlah Kalori yang Terbakar: " << total_kalori << " Kalori" << endl;
    cout << "====================================================\n";

    return 0;
}
